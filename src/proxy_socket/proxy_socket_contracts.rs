use std::usize;

use super::contracts_serializer::{self, ParseFailResult};

pub const SOCKET_CONNECT: u8 = 0;
pub const SOCKET_DISCONNECT: u8 = 1;
pub const SOCKET_PAYLOAD: u8 = 2;
pub const SOCKET_PING: u8 = 3;

const DATA_PREFIX_SIZE: usize = 6;

/*
pub fn get_packet_name(b: u8) -> String {
    if b == SOCKET_CONNECT {
        return "SOCKET_CONNECT".to_string();
    }

    if b == SOCKET_DISCONNECT {
        return "SOCKET_DISCONNECT".to_string();
    }

    if b == SOCKET_PAYLOAD {
        return "SOCKET_PAYLOAD".to_string();
    }

    if b == SOCKET_PING {
        return "SOCKET_PING".to_string();
    }

    return format!("{}", b);
}
 */

pub enum ProxySocketContract {
    Connect {
        client_connection_id: u16,
        host_port: String,
    },
    Disconnect {
        client_connection_id: u16,
    },
    Payload {
        client_connection_id: u16,
        payload: Vec<u8>,
    },
    Ping,
}

pub struct ParseOkResult {
    pub contract: ProxySocketContract,
    pub read_size: usize,
}

impl ProxySocketContract {
    pub fn deserialize(pay_load: &[u8]) -> Result<ParseOkResult, ParseFailResult> {
        let packet = contracts_serializer::deserialize_byte(&pay_load)?;

        if packet == SOCKET_PING {
            return Ok(ParseOkResult {
                contract: ProxySocketContract::Ping,
                read_size: 1,
            });
        }

        let pay_load = &pay_load[1..];

        let client_connection_id = contracts_serializer::deserialize_u16(pay_load)?;
        let pay_load = &pay_load[2..];

        if packet == SOCKET_CONNECT {
            let host_port = contracts_serializer::deserialize_string(pay_load)?;
            let read_size = host_port.len() + 4;

            let contract = ProxySocketContract::Connect {
                client_connection_id,
                host_port: host_port,
            };

            return Ok(ParseOkResult {
                contract,
                read_size,
            });
        }

        if packet == SOCKET_DISCONNECT {
            return Ok(ParseOkResult {
                contract: ProxySocketContract::Disconnect {
                    client_connection_id,
                },
                read_size: 3,
            });
        }

        if packet == SOCKET_PAYLOAD {
            let payload = contracts_serializer::deserialize_data_array(pay_load)?;
            let data_size = payload.len();

            return Ok(ParseOkResult {
                contract: ProxySocketContract::Payload {
                    client_connection_id,
                    payload,
                },
                read_size: data_size + DATA_PREFIX_SIZE,
            });
        }

        return Err(ParseFailResult::Fail);
    }

    pub fn serialize(&self) -> Vec<u8> {
        match self {
            ProxySocketContract::Connect {
                client_connection_id,
                host_port,
            } => {
                let packet_size = 4 + host_port.len();
                let mut result: Vec<u8> = Vec::with_capacity(packet_size);
                unsafe { result.set_len(packet_size) }

                result[0] = SOCKET_CONNECT;
                contracts_serializer::serialize_u16(*client_connection_id, &mut result[1..]);
                contracts_serializer::serialize_string(host_port, &mut result[3..]);
                return result;
            }

            ProxySocketContract::Disconnect {
                client_connection_id,
            } => {
                let mut result: Vec<u8> = vec![SOCKET_DISCONNECT, 0, 0];
                contracts_serializer::serialize_u16(*client_connection_id, &mut result[1..]);
                return result;
            }

            ProxySocketContract::Payload {
                client_connection_id,
                payload,
            } => {
                let package_size = DATA_PREFIX_SIZE + payload.len();

                let mut result: Vec<u8> = Vec::with_capacity(package_size);
                unsafe {
                    result.set_len(package_size);
                }

                result[0] = SOCKET_PAYLOAD;
                contracts_serializer::serialize_u16(*client_connection_id, &mut result[1..]);
                contracts_serializer::serialize_data_array(&payload, &mut result[3..]);

                return result;
            }

            ProxySocketContract::Ping => {
                return vec![SOCKET_PING];
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::ProxySocketContract;

    #[test]
    fn test_connect_contract() {
        let src = ProxySocketContract::Connect {
            client_connection_id: 13,
            host_port: "127.0.0.1:5555".to_string(),
        };

        let pay_load = src.serialize();

        println!("{:?}", pay_load);
        let dest = ProxySocketContract::deserialize(pay_load.as_slice());

        if let Ok(dest) = dest {
            assert_eq!(pay_load.len(), dest.read_size);

            if let ProxySocketContract::Connect {
                client_connection_id,
                host_port,
            } = dest.contract
            {
                assert_eq!(client_connection_id, 13);
                assert_eq!(host_port, "127.0.0.1:5555");
            } else {
                panic!("Invalid result");
            }
            return;
        }

        if let Err(err) = dest {
            panic!("{:?}", err);
        }
    }

    #[test]
    fn test_disconnect_contract() {
        let src = ProxySocketContract::Disconnect {
            client_connection_id: 13,
        };

        let pay_load = src.serialize();

        let dest = ProxySocketContract::deserialize(pay_load.as_slice()).unwrap();

        if let ProxySocketContract::Disconnect {
            client_connection_id,
        } = dest.contract
        {
            assert_eq!(client_connection_id, 13);
        } else {
            panic!("Invalid result");
        }
    }

    #[test]
    fn test_ping_contract() {
        let src = ProxySocketContract::Ping;

        let pay_load = src.serialize();

        let dest = ProxySocketContract::deserialize(pay_load.as_slice()).unwrap();

        if let ProxySocketContract::Ping = dest.contract {
            assert_eq!(pay_load.len(), 1);
        } else {
            panic!("Invalid result");
        }
    }

    #[test]
    fn test_payload_contract() {
        let src = ProxySocketContract::Payload {
            client_connection_id: 13,
            payload: vec![1, 2, 3],
        };

        let result_payload = src.serialize();

        let dest = ProxySocketContract::deserialize(result_payload.as_slice()).unwrap();

        if let ProxySocketContract::Payload {
            client_connection_id,
            payload,
        } = dest.contract
        {
            assert_eq!(client_connection_id, 13);
            assert_eq!(3, payload.len());
            assert_eq!(1, payload[0]);
            assert_eq!(2, payload[1]);
            assert_eq!(3, payload[2]);
        } else {
            panic!("Invalid result");
        }
    }
}

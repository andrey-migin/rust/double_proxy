pub struct BufferReader {
    buffer: Vec<u8>,
    pub start_pos: usize,
    pub end_pos: usize,
}

impl BufferReader {
    pub fn new(size: usize) -> Self {
        let mut result = Self {
            buffer: Vec::with_capacity(size),
            start_pos: 0,
            end_pos: 0,
        };

        result.buffer.resize(size, 0);

        return result;
    }

    pub fn borrow_mut(&mut self) -> &mut [u8] {
        self.gc();
        return &mut self.buffer[self.end_pos..];
    }

    pub fn commit_written_size(&mut self, size: usize) {
        self.end_pos += size;
    }

    pub fn get_buffer_to_use(&self) -> Option<&[u8]> {
        if self.start_pos == self.end_pos {
            return None;
        }
        return Some(&self.buffer[self.start_pos..self.end_pos]);
    }

    pub fn commit_read_size(&mut self, size: usize) {
        self.start_pos += size;

        if self.start_pos == self.end_pos {
            self.start_pos = 0;
            self.end_pos = 0;
        }
    }
    pub fn gc(&mut self) {
        if self.start_pos == 0 || self.end_pos == 0 {
            return;
        }

        let len = self.end_pos - self.start_pos;

        let mut mem: Vec<u8> = Vec::with_capacity(len);

        unsafe { mem.set_len(len) };

        let buf = &self.buffer[self.start_pos..self.end_pos];

        mem.as_mut_slice().copy_from_slice(buf);

        self.buffer[0..len].copy_from_slice(mem.as_slice());

        self.start_pos = 0;
        self.end_pos = len;
    }
}

#[cfg(test)]
mod tests {
    use super::BufferReader;

    #[test]
    fn test_general_case() {
        let mut buffer = BufferReader::new(10);

        let data = buffer.borrow_mut();
        assert_eq!(data.len(), 10);

        data[0] = 1;
        data[1] = 2;
        data[2] = 3;
        buffer.commit_written_size(3);

        let data = buffer.borrow_mut();
        assert_eq!(data.len(), 7);

        data[0] = 4;
        data[1] = 5;
        data[2] = 6;
        buffer.commit_written_size(3);

        let data_to_use = buffer.get_buffer_to_use().expect("Error");

        assert_eq!(data_to_use.len(), 6);

        assert_eq!(data_to_use[0], 1);
        assert_eq!(data_to_use[1], 2);
        assert_eq!(data_to_use[2], 3);
        assert_eq!(data_to_use[3], 4);
        assert_eq!(data_to_use[4], 5);
        assert_eq!(data_to_use[5], 6);

        buffer.commit_read_size(4);

        let data_to_use = buffer.get_buffer_to_use().expect("Error");
        assert_eq!(data_to_use.len(), 2);

        assert_eq!(data_to_use[0], 5);
        assert_eq!(data_to_use[1], 6);
    }

    #[test]
    fn test_gc_case() {
        let mut buffer = BufferReader::new(10);

        let data = buffer.borrow_mut();

        data[0] = 1;
        data[1] = 2;
        data[2] = 3;
        data[3] = 4;
        data[4] = 5;
        data[5] = 6;
        buffer.commit_written_size(6);

        buffer.commit_read_size(2);
        assert_eq!(buffer.start_pos, 2);
        assert_eq!(buffer.end_pos, 6);

        buffer.gc();

        println!("{:?}", buffer.buffer);

        assert_eq!(buffer.start_pos, 0);
        assert_eq!(buffer.end_pos, 4);

        let data_to_use = buffer.get_buffer_to_use().expect("Error");

        assert_eq!(data_to_use[0], 3);
        assert_eq!(data_to_use[1], 4);
        assert_eq!(data_to_use[2], 5);
        assert_eq!(data_to_use[3], 6);
    }
}

use crate::{app::server_app_ctx::ServerAppCtx, common::events::*, server_connections::events::*};
use async_std::net::{TcpListener, TcpStream};
use futures::{AsyncReadExt, AsyncWriteExt, StreamExt};
use std::{collections::HashMap, sync::Arc};

pub async fn start(
    host_port: String,
    client_host_port: String,
    app_ctx: Arc<ServerAppCtx>,
    pub_socket_receiver: Receiver<PublicSocketEvent>,
) {
    let listener = TcpListener::bind(host_port.as_str()).await.unwrap();

    println!("Listening to Server socket: {}", host_port.as_str());

    let mut incoming = listener.incoming();

    let mut socket_id: u16 = 0;

    async_std::task::spawn(public_sockets_events_loop(
        pub_socket_receiver,
        app_ctx.clone(),
    ));

    loop {
        let incoming_result = incoming.next().await;

        if incoming_result.is_none() {
            println!(
                "Can not accept service server socket {}",
                host_port.as_str()
            );
            continue;
        }

        socket_id = socket_id + 1;

        let stream_result = incoming_result.unwrap();

        let tcp_stream = Arc::new(stream_result.unwrap());

        app_ctx
            .client_socket_events_sender
            .unbounded_send(PublicSocketEvent::Connect {
                client_connection_id: socket_id,
                tcp_stream: tcp_stream.clone(),
            })
            .unwrap();

        // Start Read Thread for the socket

        async_std::task::spawn(tcp_listeners_read_loop(
            socket_id,
            tcp_stream,
            app_ctx.clone(),
        ));

        app_ctx.send_connect_trough_proxy_socket(socket_id, client_host_port.clone());
    }
}

async fn public_sockets_events_loop(
    mut public_listen_socket_receiver: Receiver<PublicSocketEvent>,
    app_ctx: Arc<ServerAppCtx>,
) {
    let mut sockets: HashMap<u16, Arc<TcpStream>> = HashMap::new();

    while let Some(msg) = public_listen_socket_receiver.next().await {
        match msg {
            PublicSocketEvent::Connect {
                client_connection_id,
                tcp_stream,
            } => {
                println!("Service socket {}: Connected ", client_connection_id);
                sockets.insert(client_connection_id, tcp_stream);
            }
            PublicSocketEvent::Disconnect {
                client_connection_id,
            } => {
                println!("Service socket {}: Disconnected", client_connection_id);
                if let Some(tcp_stream) = sockets.remove(&client_connection_id) {
                    let result = tcp_stream.as_ref().close().await;
                    if let Err(err) = result {
                        println!(
                            "Service socket {}: Can not close stream with the reason: {:?}",
                            client_connection_id, err
                        );
                    }
                    app_ctx.send_disconnect_trough_proxy_socket(client_connection_id);
                }
            }
            PublicSocketEvent::SendData {
                client_connection_id,
                payload,
            } => {
                if let Some(socket) = sockets.get_mut(&client_connection_id) {
                    let result = socket.as_ref().write_all(&payload).await;

                    if let Err(err) = result {
                        println!(
                            "Service socket {}: Can not send payload with reason: {:?}",
                            client_connection_id, err
                        );

                        app_ctx.disconnect_public_socket(
                            client_connection_id,
                            "PublicSocketEvent::SendData",
                        );
                    }
                }
            }

            PublicSocketEvent::ProxySocketIsDisconnected => {
                for tcp_stream in &sockets {
                    let result = tcp_stream.1.as_ref().close().await;

                    if let Err(err) = result {
                        println!(
                            "Can not disconnect service socket {}. Reason: {:?}",
                            tcp_stream.0, err
                        );
                    }
                }

                sockets.clear();
            }
        }
    }
}

async fn tcp_listeners_read_loop(
    connection_id: u16,
    tcp_stream: Arc<TcpStream>,
    app_ctx: Arc<ServerAppCtx>,
) {
    let mut buffer = Vec::<u8>::with_capacity(crate::common::consts::BUFFER_SIZE);
    unsafe { buffer.set_len(crate::common::consts::BUFFER_SIZE) }
    let mut tcp_stream = &*tcp_stream.as_ref();

    loop {
        match tcp_stream.read(&mut buffer).await {
            Ok(read_size) => {
                if read_size == 0 {
                    println!(
                        "Service Socket {}: Read size is 0. Disconnecting",
                        connection_id
                    );
                    app_ctx.disconnect_public_socket(connection_id, "public_socket_read_loop-1");
                    break;
                } else {
                    let read_data = &buffer[0..read_size];
                    app_ctx.send_payload_though_proxy_socket(connection_id, read_data.to_vec());
                }
            }
            Err(err) => {
                println!(
                    "Service Socket {}: Can not read data. Reason: {:?}",
                    connection_id, err
                );

                app_ctx.disconnect_public_socket(connection_id, "public_socket_read_loop-2");
                break;
            }
        }
    }
}

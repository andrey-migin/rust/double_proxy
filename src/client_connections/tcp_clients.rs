use std::{collections::HashMap, sync::Arc};

use async_std::net::TcpStream;
use futures::{AsyncReadExt, AsyncWriteExt, StreamExt};

use crate::{app::client_app_ctx::ClientAppCtx, common::events::Receiver};

use super::events::TcpClientSocketCommand;

pub async fn start(
    mut client_socket_commands: Receiver<TcpClientSocketCommand>,
    app_ctx: Arc<ClientAppCtx>,
) {
    let mut sockets: HashMap<u16, Arc<TcpStream>> = HashMap::new();

    while let Some(msg) = client_socket_commands.next().await {
        match msg {
            TcpClientSocketCommand::Connect {
                client_connection_id,
                host_port,
            } => {
                println!(
                    "Service socket {}: Connecting to the {}",
                    client_connection_id,
                    host_port.as_str()
                );
                let tcp_stream = TcpStream::connect(host_port.as_str()).await;

                if let Err(err) = tcp_stream {
                    println!(
                        "Service socket {}: Can not connect to the service {}. Reason: {:?}",
                        client_connection_id,
                        host_port.as_str(),
                        err
                    );

                    app_ctx.send_client_disconnect_through_proxy_socket(client_connection_id);
                } else {
                    println!(
                        "Service socket {}: Connected to {}",
                        client_connection_id, host_port
                    );

                    let tcp_stream = Arc::new(tcp_stream.unwrap());

                    sockets.insert(client_connection_id, tcp_stream.clone());

                    // Start Read Thread for the socket
                    async_std::task::spawn(read_socket_loop(
                        client_connection_id,
                        tcp_stream,
                        app_ctx.clone(),
                    ));
                }
            }
            TcpClientSocketCommand::Disconnect {
                client_connection_id,
            } => {
                let tcp_stream_result = sockets.remove(&client_connection_id);

                println!("Service socket {}: Disconnected", &client_connection_id);

                if let Some(tcp_stream) = tcp_stream_result {
                    let result = tcp_stream.as_ref().close().await;

                    if let Err(err) = result {
                        println!(
                            "Could not close client connection {} with error {:?}",
                            client_connection_id, err
                        );
                    }
                }
            }

            TcpClientSocketCommand::SendRawPayload {
                client_connection_id,
                raw_data,
            } => {
                let tcp_stream_result = sockets.get(&client_connection_id);

                match tcp_stream_result {
                    Some(tcp_stream) => {
                        let result = tcp_stream.as_ref().write_all(raw_data.as_ref()).await;

                        if let Err(err) = result {
                            println!(
                                "Can not write payload to the client socket {}. Reason: {:?}",
                                client_connection_id, err
                            )
                        }
                    }
                    None => {
                        println!(
                            "Attempt to send payload to client socket {} which is not exists",
                            client_connection_id
                        );

                        app_ctx.send_client_disconnect_through_proxy_socket(client_connection_id);
                    }
                }
            }

            TcpClientSocketCommand::ProxySocketIsDisconnected => {
                for tcp_stream in &sockets {
                    let result = tcp_stream.1.as_ref().close().await;

                    if let Err(err) = result {
                        println!(
                            "Can not disconnect socket {}. Reason: {:?}",
                            tcp_stream.0, err
                        );
                    }
                }

                sockets.clear();
            }
        }
    }
}

async fn read_socket_loop(
    client_connection_id: u16,
    tcp_stream: Arc<TcpStream>,
    app_ctx: Arc<ClientAppCtx>,
) {
    let mut buffer = Vec::<u8>::with_capacity(crate::common::consts::BUFFER_SIZE);
    unsafe { buffer.set_len(crate::common::consts::BUFFER_SIZE) }
    let mut tcp_stream = &*tcp_stream.as_ref();

    loop {
        match tcp_stream.read(&mut buffer).await {
            Ok(read_size) => {
                if read_size == 0 {
                    println!(
                        "Socket {} is disconnected. Read size is 0",
                        client_connection_id
                    );
                    app_ctx.disconnect_client_connection(client_connection_id);
                    break;
                } else {
                    let read_data = &buffer[0..read_size];
                    app_ctx.send_client_payload_though_proxy_socket(
                        client_connection_id,
                        read_data.to_vec(),
                    );
                }
            }
            Err(err) => {
                println!(
                    "Can not read data from the socket: {}. Err: {}",
                    client_connection_id, err
                );

                app_ctx.disconnect_client_connection(client_connection_id);
                break;
            }
        }
    }
}

use std::sync::Arc;

use async_std::net::TcpStream;

pub enum PublicSocketEvent {
    Connect {
        client_connection_id: u16,
        tcp_stream: Arc<TcpStream>,
    },
    Disconnect {
        client_connection_id: u16,
    },
    SendData {
        client_connection_id: u16,
        payload: Vec<u8>,
    },

    ProxySocketIsDisconnected,
}

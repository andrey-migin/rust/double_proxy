use std::sync::Arc;

use async_std::net::TcpStream;
pub enum ProxySocketEvent {
    ProxySocketConnected {
        proxy_socket_id: u16,
        tcp_stream: Arc<TcpStream>,
    },
    ProxySocketDisconnected {
        proxy_socket_id: u16,
    },
    RawPayload {
        client_connection_id: u16,
        payload: Vec<u8>,
    },
}

mod app;
mod client_connections;
mod common;
mod proxy_socket;
mod server_connections;

use app::{client_app_ctx::ClientAppCtx, server_app_ctx::*};
use futures::channel::mpsc;
use proxy_socket::proxy_tcp_client;
use std::{env, sync::Arc};

#[async_std::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!();
        println!("Program can be run in two modes. Server and Client");
        println!("double_proxy -s xxxxxx:xxxx x.x.x.x:xxxx x.x.x.x:xxxx");
        println!("double_proxy -c xxxxxx:xxxx");

        return;
    }

    if args[1] == "-s" {
        if args.len() < 5 {
            println!();
            println!("Server: double_proxy -s 10.0.0.1:1555 10.0.0.1:1556 127.0.0.1:22");
            println!("                        Server        Proxy         Client");
            println!();
            return;
        }

        start_server(&args[2], &args[3], &args[4]).await;
        return;
    }

    if args[1] == "-c" {
        if args.len() < 3 {
            println!("Client: double_proxy -c 10.0.0.1:1556");
            println!("                        Proxy");
            println!();
            return;
        }

        start_client(&args[2]).await;
        return;
    }

    println!("Second parameter must be server or client");
}

async fn start_server(service_host_port: &str, proxy_host_port: &str, client_host_port: &str) {
    let (client_socket_sender, client_socket_receiver) = mpsc::unbounded();

    let (proxy_socket_sender, proxy_socket_receiver) = mpsc::unbounded();

    let app_ctx = ServerAppCtx::new(client_socket_sender, proxy_socket_sender);

    let app_ctx = Arc::new(app_ctx);

    println!("Remote host_port is: {}", client_host_port);

    let client_listen_socket_task =
        async_std::task::spawn(server_connections::tcp_listener::start(
            service_host_port.to_string(),
            client_host_port.to_string(),
            app_ctx.clone(),
            client_socket_receiver,
        ));

    let proxy_listen_socket_task = async_std::task::spawn(proxy_socket::proxy_tcp_listener::start(
        proxy_host_port.to_string(),
        app_ctx,
        proxy_socket_receiver,
    ));

    client_listen_socket_task.await;
    proxy_listen_socket_task.await;
}

async fn start_client(proxy_host_port: &str) {
    let (client_socket_sender, client_socket_receiver) = mpsc::unbounded();

    let (proxy_socket_sender, proxy_socket_receiver) = mpsc::unbounded();

    let app_ctx = ClientAppCtx::new(client_socket_sender, proxy_socket_sender);

    let app_ctx = Arc::new(app_ctx);

    let client_socket_task = async_std::task::spawn(client_connections::tcp_clients::start(
        client_socket_receiver,
        app_ctx.clone(),
    ));

    let proxy_socket_task = async_std::task::spawn(proxy_tcp_client::start(
        proxy_host_port.to_string(),
        app_ctx,
        proxy_socket_receiver,
    ));

    client_socket_task.await;
    proxy_socket_task.await;
}

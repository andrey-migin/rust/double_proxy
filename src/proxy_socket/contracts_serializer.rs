#[derive(Debug)]
pub enum ParseFailResult {
    NotEnoughDataToReadWrite,
    Fail,
}

pub fn deserialize_byte(data: &[u8]) -> Result<u8, ParseFailResult> {
    if data.len() < 1 {
        return Err(ParseFailResult::NotEnoughDataToReadWrite);
    }

    return Ok(data[0]);
}

/* ----------- U16 (3 bytes) --------------- */

pub fn serialize_u16(value: u16, buffer: &mut [u8]) {
    buffer[0] = value as u8;
    buffer[1] = (value / 256) as u8;
}

pub fn deserialize_u16(data: &[u8]) -> Result<u16, ParseFailResult> {
    if data.len() < 2 {
        return Err(ParseFailResult::NotEnoughDataToReadWrite);
    }

    return Ok(data[0] as u16 + data[1] as u16 * 256);
}

/* ----------- DATA_LEN (3 bytes) --------------- */

pub fn serialize_data_len(value: usize, buffer: &mut [u8]) {
    buffer[0] = value as u8;
    buffer[1] = (value >> 8) as u8;
    buffer[2] = (value >> 16) as u8;
}

pub fn deserialize_data_len(data: &[u8]) -> Result<usize, ParseFailResult> {
    if data.len() < 3 {
        return Err(ParseFailResult::NotEnoughDataToReadWrite);
    }

    let result = data[0] as usize + data[1] as usize * 256 + data[2] as usize * 256 * 256;

    return Ok(result);
}

/* ----------- DATA ARRAY --------------- */
pub fn serialize_data_array(value: &[u8], buffer: &mut [u8]) {
    serialize_data_len(value.len(), buffer);
    &buffer[3..3 + value.len()].copy_from_slice(value);
}

pub fn deserialize_data_array(data: &[u8]) -> Result<Vec<u8>, ParseFailResult> {
    let data_len = deserialize_data_len(data)?;

    let data = &data[3..];

    if data.len() < data_len {
        return Err(ParseFailResult::NotEnoughDataToReadWrite);
    }

    return Ok(data[..data_len].to_vec());
}

/* ----------- PASCAL STRING --------------- */
pub fn deserialize_string(data: &[u8]) -> Result<String, ParseFailResult> {
    if data.len() == 1 {
        return Err(ParseFailResult::NotEnoughDataToReadWrite);
    }

    let len = data[0] as usize;

    let data = &data[1..];

    if data.len() < len {
        return Err(ParseFailResult::NotEnoughDataToReadWrite);
    }

    let str = &data[..len];

    match String::from_utf8(str.to_vec()) {
        Ok(result) => Ok(result),
        Err(_) => Err(ParseFailResult::NotEnoughDataToReadWrite),
    }
}

pub fn serialize_string(value: &str, buffer: &mut [u8]) {
    buffer[0] = value.len() as u8;

    let str_as_bytes = value.as_bytes();

    &buffer[1..value.len() + 1].copy_from_slice(str_as_bytes);
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::Rng;

    #[test]
    fn test_u16() {
        let mut res: Vec<u8> = vec![0, 0];

        serialize_u16(255, &mut res);
        assert_eq!(255, res[0]);
        assert_eq!(0, res[1]);

        serialize_u16(256, &mut res);
        assert_eq!(0, res[0]);
        assert_eq!(1, res[1]);

        for i in 0u16..65535u16 {
            serialize_u16(i, &mut res);
            let res = deserialize_u16(&res).unwrap();
            assert_eq!(i, res);
        }

        serialize_u16(256, &mut res);
    }

    #[test]
    fn test_data_size() {
        let mut data = vec![0u8, 0u8, 0u8];

        let mut rng = rand::thread_rng();
        for _ in 0..65535 {
            let value = rng.gen_range(0..255 * 255 * 255) as usize;
            serialize_data_len(value, &mut data);
            let result = deserialize_data_len(&data).unwrap();

            assert_eq!(value, result);
        }
    }

    #[test]
    fn test_string() {
        let mut buffer = vec![0u8; 65535];

        let value = "Test String";

        serialize_string(value, &mut buffer);

        let result = deserialize_string(&buffer).unwrap();

        assert_eq!(value, result);
    }

    #[test]
    fn test_array() {
        let mut buffer = vec![0u8; 65535];

        let value = vec![1u8, 2u8, 3u8];

        serialize_data_array(&value, &mut buffer);

        let result = deserialize_data_array(&buffer).unwrap();

        assert_eq!(value.len(), result.len());

        for i in 0..value.len() {
            assert_eq!(value[i], result[i]);
        }
    }
}

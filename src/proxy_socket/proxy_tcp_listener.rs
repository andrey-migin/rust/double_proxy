use crate::{app::server_app_ctx::ServerAppCtx, proxy_socket::events::*};

use async_std::net::{TcpListener, TcpStream};
use futures::{AsyncReadExt, AsyncWriteExt, StreamExt};
use std::sync::Arc;

use super::{
    super::common::events::*, contracts_serializer::ParseFailResult, proxy_socket_contracts::*,
};

use super::buffer_reader::BufferReader;

pub async fn start(
    host_port: String,
    app_ctx: Arc<ServerAppCtx>,
    proxy_socket_receiver: Receiver<ProxySocketEvent>,
) {
    let listener = TcpListener::bind(host_port.as_str()).await.unwrap();
    let mut incoming = listener.incoming();

    let mut socket_id: u16 = 0;

    async_std::task::spawn(proxy_sockets_events_loop(
        proxy_socket_receiver,
        app_ctx.clone(),
    ));

    println!("Listening Proxy socket: {}", host_port.as_str());
    loop {
        let incoming_result = incoming.next().await;

        if incoming_result.is_none() {
            println!("Can not accept Public server socket {}", host_port.as_str());
            continue;
        }

        let stream_result = incoming_result.unwrap();

        let tcp_stream = Arc::new(stream_result.unwrap());
        socket_id = socket_id + 1;

        // Inform write thread that we have new socket to write
        app_ctx
            .as_ref()
            .proxy_socket_events_sender
            .unbounded_send(ProxySocketEvent::ProxySocketConnected {
                proxy_socket_id: socket_id,
                tcp_stream: tcp_stream.clone(),
            })
            .unwrap();

        // Start Read Thread for the socket
        async_std::task::spawn(proxy_sockets_read_loop(
            tcp_stream,
            app_ctx.clone(),
            socket_id,
        ));
    }
}

async fn proxy_sockets_events_loop(
    mut proxy_socket_events: Receiver<ProxySocketEvent>,
    app_ctx: Arc<ServerAppCtx>,
) {
    let mut proxy_socket: Option<Arc<TcpStream>> = None;
    let mut current_socket_id: u16 = 0;

    while let Some(msg) = proxy_socket_events.next().await {
        match msg {
            ProxySocketEvent::ProxySocketConnected {
                proxy_socket_id,
                tcp_stream,
            } => {
                println!("Proxy socket {}: Connected", proxy_socket_id);
                proxy_socket = Some(tcp_stream);
                current_socket_id = proxy_socket_id;
                app_ctx.proxy_server_has_connection();
            }
            ProxySocketEvent::ProxySocketDisconnected { proxy_socket_id } => {
                println!("Proxy socket {}: Disconnected", proxy_socket_id);
                if current_socket_id == proxy_socket_id {
                    if let Some(tcp_stream) = proxy_socket {
                        let result = tcp_stream.as_ref().close().await;
                        if let Err(err) = result {
                            println!(
                                "Error happend during socket {} shutdown. Reason: {}",
                                proxy_socket_id, err
                            );
                        }
                    }

                    proxy_socket = None;
                    app_ctx.proxy_server_is_disconnected();
                }
            }
            ProxySocketEvent::RawPayload {
                client_connection_id,
                payload,
            } => {
                if let Some(tcp_stream) = &proxy_socket {
                    let result = tcp_stream.as_ref().write_all(&payload).await;

                    //ToDo - Debug
                    /*
                                       println!(
                                           "Proxy: Server->Client, payload {} to socket {}. Len: {}",
                                           get_packet_name(payload[0]),
                                           client_connection_id,
                                           payload.len()
                                       );
                    */
                    if result.is_err() {
                        println!("Could send message through write to the proxy socket {} for public socket {}", current_socket_id, client_connection_id);
                    }
                } else {
                    app_ctx.disconnect_public_socket(
                        client_connection_id,
                        "ProxySocketEvent::RawPayload",
                    );
                    println!("Attempt to send to the public socket {} is failed. There is not proxy socket connection established", client_connection_id);
                }
            }
        }
    }
}

async fn proxy_sockets_read_loop(
    tcp_stream: Arc<TcpStream>,
    app_ctx: Arc<ServerAppCtx>,
    connection_id: u16,
) {
    let mut buffer_reader = BufferReader::new(crate::common::consts::BUFFER_SIZE * 2);

    let mut tcp_stream: &TcpStream = &*tcp_stream.as_ref();

    loop {
        let buffer_to_read = buffer_reader.borrow_mut();
        match tcp_stream.read(buffer_to_read).await {
            Ok(read_size) => {
                if read_size == 0 {
                    println!(
                        "Proxy socket is disconnected. Read size is 0. ConnectionId: {}",
                        connection_id
                    );

                    app_ctx.disconnect_proxy_socket(connection_id);

                    break;
                }

                buffer_reader.commit_written_size(read_size);
            }
            Err(err) => {
                println!(
                    "Can not read data from the Proxy socket{}. Err:{}",
                    connection_id, err
                );
                app_ctx.disconnect_proxy_socket(connection_id);
                break;
            }
        }

        let mut borrowed_buffer = buffer_reader.get_buffer_to_use();

        while let Some(buffer) = borrowed_buffer {
            let parse_result = ProxySocketContract::deserialize(buffer);

            match parse_result {
                Ok(packet) => {
                    buffer_reader.commit_read_size(packet.read_size);

                    match packet.contract {
                        ProxySocketContract::Payload {
                            client_connection_id,
                            payload,
                        } => {
                            app_ctx
                                .as_ref()
                                .send_payload_to_server_socket(client_connection_id, payload);
                        }
                        ProxySocketContract::Disconnect {
                            client_connection_id,
                        } => {
                            app_ctx.as_ref().disconnect_public_socket(
                                client_connection_id,
                                "proxy_tcp_listner.proxy_sockets_read_loop",
                            );
                        }
                        _ => {
                            let ping_payload = ProxySocketContract::Ping.serialize();

                            if let Err(err) = tcp_stream.write_all(&ping_payload).await {
                                println!(
                                    "Can not send pong to the socket {}. Disconnecting. Err:{:?}",
                                    connection_id, err
                                );
                                return;
                            }
                        }
                    }
                }
                Err(err) => match err {
                    ParseFailResult::Fail => {
                        println!(
                            "proxy_sockets_read_loop: Socket is disconnected because wrong data in the socket"                            
                        );

                        app_ctx.as_ref().disconnect_proxy_socket(connection_id);
                        return;
                    }
                    ParseFailResult::NotEnoughDataToReadWrite => {
                        break;
                    }
                },
            }

            borrowed_buffer = buffer_reader.get_buffer_to_use();
        }
    }
}

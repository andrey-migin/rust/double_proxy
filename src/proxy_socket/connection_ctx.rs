use std::time::SystemTime;

use async_std::sync::RwLock;

struct ConnectionData {
    connected: bool,
    last_read_time: SystemTime,
}

pub struct ConnectionContext {
    data: RwLock<ConnectionData>,
    pub id: u16,
}

impl ConnectionContext {
    pub fn new(id: u16) -> Self {
        let data = ConnectionData {
            connected: true,
            last_read_time: SystemTime::now(),
        };

        Self {
            data: RwLock::new(data),
            id,
        }
    }
    pub async fn is_connected(&self) -> bool {
        let data = self.data.read().await;
        return data.connected;
    }

    pub async fn disconnect(&self) {
        let mut data = self.data.write().await;
        data.connected = true
    }

    pub async fn update_last_read_time(&self) {
        let mut connection_ctx_mut_access = self.data.write().await;
        connection_ctx_mut_access.last_read_time = SystemTime::now();
    }

    pub async fn get_last_read_seconds(&self) -> u64 {
        let now = SystemTime::now();

        let data = self.data.read().await;
        let duration = now.duration_since(data.last_read_time).unwrap();
        return duration.as_secs();
    }
}

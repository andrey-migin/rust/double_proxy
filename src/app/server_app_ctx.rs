use std::sync::atomic::AtomicBool;

use crate::{common::events::*, proxy_socket::events::*, server_connections::events::*};

pub struct ServerAppCtx {
    pub client_socket_events_sender: Sender<PublicSocketEvent>,
    pub proxy_socket_events_sender: Sender<ProxySocketEvent>,

    has_proxy_connection: AtomicBool,
}

impl ServerAppCtx {
    pub fn new(
        client_socket_events_sender: Sender<PublicSocketEvent>,
        proxy_socket_events_sender: Sender<ProxySocketEvent>,
    ) -> Self {
        Self {
            client_socket_events_sender,
            proxy_socket_events_sender,
            has_proxy_connection: AtomicBool::new(false),
        }
    }
    pub fn proxy_server_has_connection(&self) {
        self.has_proxy_connection
            .store(true, std::sync::atomic::Ordering::Release);
    }

    pub fn proxy_server_is_disconnected(&self) {
        let result = self
            .client_socket_events_sender
            .unbounded_send(PublicSocketEvent::ProxySocketIsDisconnected);

        if let Err(err) = result {
            println!(
                "Can not post proxy socket is disconnected to public socket. Err {:?}",
                err
            );
        }

        self.has_proxy_connection
            .store(false, std::sync::atomic::Ordering::Release);
    }

    pub fn send_payload_to_server_socket(&self, client_connection_id: u16, payload: Vec<u8>) {
        let result = self
            .client_socket_events_sender
            .unbounded_send(PublicSocketEvent::SendData {
                client_connection_id,
                payload,
            });

        if let Err(err) = result {
            println!(
                "Can not send payload to the public socket {}: {:?}",
                client_connection_id, err
            );
        }
    }

    pub fn disconnect_public_socket(&self, client_connection_id: u16, process: &str) {
        let result =
            self.client_socket_events_sender
                .unbounded_send(PublicSocketEvent::Disconnect {
                    client_connection_id,
                });

        println!(
            "Send Disconnect {}. Process: {}",
            client_connection_id, process
        );

        if let Err(err) = result {
            println!(
                "Can not disconnect public socket {}: {:?}",
                client_connection_id, err
            );
        }
    }

    /// Proxy Socket Commands

    pub fn disconnect_proxy_socket(&self, proxy_socket_id: u16) {
        let result = self
            .proxy_socket_events_sender
            .unbounded_send(ProxySocketEvent::ProxySocketDisconnected { proxy_socket_id });

        if let Err(err) = result {
            println!(
                "Can not send disconnect the proxy socket {}. Err:{:?}",
                proxy_socket_id, err
            );
        }
    }

    pub fn send_connect_trough_proxy_socket(&self, client_connection_id: u16, host_port: String) {
        let tcp_contract =
            crate::proxy_socket::proxy_socket_contracts::ProxySocketContract::Connect {
                client_connection_id,
                host_port,
            };

        let payload = tcp_contract.serialize();

        self.post_client_event_to_proxy_socket(client_connection_id, payload);
    }

    pub fn send_disconnect_trough_proxy_socket(&self, client_connection_id: u16) {
        let tcp_contract =
            crate::proxy_socket::proxy_socket_contracts::ProxySocketContract::Disconnect {
                client_connection_id,
            };

        let payload = tcp_contract.serialize();

        self.post_client_event_to_proxy_socket(client_connection_id, payload);
    }

    pub fn send_payload_though_proxy_socket(&self, client_connection_id: u16, payload: Vec<u8>) {
        let tcp_contract =
            crate::proxy_socket::proxy_socket_contracts::ProxySocketContract::Payload {
                client_connection_id,
                payload,
            };

        let payload = tcp_contract.serialize();

        self.post_client_event_to_proxy_socket(client_connection_id, payload);
    }

    fn post_client_event_to_proxy_socket(&self, client_connection_id: u16, payload: Vec<u8>) {
        let result = self
            .proxy_socket_events_sender
            .unbounded_send(ProxySocketEvent::RawPayload {
                client_connection_id,
                payload,
            });

        if let Err(err) = result {
            println!(
                "Can not send disconnect through the proxy socket to the public socket {}. Err:{:?}",
                client_connection_id, err
            );
        }
    }
}

use std::sync::{atomic::AtomicBool, Arc};

use async_std::net::TcpStream;

use crate::{
    client_connections::events::TcpClientSocketCommand, common::events::Sender,
    proxy_socket::events::ProxySocketEvent,
};

pub struct ClientAppCtx {
    pub client_socket_commands_sender: Sender<TcpClientSocketCommand>,
    pub proxy_socket_events_sender: Sender<ProxySocketEvent>,
    has_proxy_connection: AtomicBool,
}

impl ClientAppCtx {
    pub fn new(
        client_socket_commands_sender: Sender<TcpClientSocketCommand>,
        proxy_socket_events_sender: Sender<ProxySocketEvent>,
    ) -> Self {
        Self {
            client_socket_commands_sender,
            proxy_socket_events_sender,
            has_proxy_connection: AtomicBool::new(false),
        }
    }

    /// Client Socket
    pub fn start_new_client_connection(&self, client_connection_id: u16, host_port: String) {
        let result =
            self.client_socket_commands_sender
                .unbounded_send(TcpClientSocketCommand::Connect {
                    client_connection_id,
                    host_port,
                });

        if let Err(err) = result {
            println!(
                "Can not post 'new connection' command to client socket  {}: {:?}",
                client_connection_id, err
            );
        }
    }

    pub fn disconnect_client_connection(&self, client_connection_id: u16) {
        let result =
            self.client_socket_commands_sender
                .unbounded_send(TcpClientSocketCommand::Disconnect {
                    client_connection_id,
                });

        if let Err(err) = result {
            println!(
                "Can not post 'disconnect' command to client socket {}. {:?}",
                client_connection_id, err
            );
        }
    }

    pub fn send_payload_to_client_connection(&self, client_connection_id: u16, raw_data: Vec<u8>) {
        let result = self.client_socket_commands_sender.unbounded_send(
            TcpClientSocketCommand::SendRawPayload {
                client_connection_id,
                raw_data,
            },
        );

        if let Err(err) = result {
            println!(
                "Can not post 'send payload' event for public socket {}. Err:{:?}",
                client_connection_id, err
            );
        }
    }

    ///Proxy Socket

    pub fn proxy_server_is_connected(&self, proxy_socket_id: u16, tcp_stream: Arc<TcpStream>) {
        let result = self.proxy_socket_events_sender.unbounded_send(
            ProxySocketEvent::ProxySocketConnected {
                proxy_socket_id,
                tcp_stream,
            },
        );

        if let Err(err) = result {
            println!("Can not post Proxy socket is connected. Err:{:?}", err);
        }

        self.has_proxy_connection
            .store(true, std::sync::atomic::Ordering::Release);
    }

    pub fn proxy_server_is_disconnected(&self, proxy_socket_id: u16) {
        let result = self
            .client_socket_commands_sender
            .unbounded_send(TcpClientSocketCommand::ProxySocketIsDisconnected);

        if let Err(err) = result {
            println!(
                "Can not post 'Proxy socket is disconnected' event for public socket. Err:{:?}",
                err
            );
        }

        let result = self
            .proxy_socket_events_sender
            .unbounded_send(ProxySocketEvent::ProxySocketDisconnected { proxy_socket_id });

        if let Err(err) = result {
            println!("Can not post Proxy socket is connected. Err:{:?}", err);
        }
        self.has_proxy_connection
            .store(false, std::sync::atomic::Ordering::Release);
    }

    pub fn send_client_disconnect_through_proxy_socket(&self, client_connection_id: u16) {
        let tcp_contract =
            crate::proxy_socket::proxy_socket_contracts::ProxySocketContract::Disconnect {
                client_connection_id,
            };

        let payload = tcp_contract.serialize();

        self.send_raw_payload_to_proxy_socket(client_connection_id, payload);
    }

    pub fn send_client_payload_though_proxy_socket(
        &self,
        client_connection_id: u16,
        payload: Vec<u8>,
    ) {
        let tcp_contract =
            crate::proxy_socket::proxy_socket_contracts::ProxySocketContract::Payload {
                client_connection_id,
                payload,
            };

        let payload = tcp_contract.serialize();

        self.send_raw_payload_to_proxy_socket(client_connection_id, payload);
    }

    fn send_raw_payload_to_proxy_socket(&self, client_connection_id: u16, payload: Vec<u8>) {
        let result = self
            .proxy_socket_events_sender
            .unbounded_send(ProxySocketEvent::RawPayload {
                client_connection_id,
                payload,
            });

        if let Err(err) = result {
            println!(
                "Can not post payload event for public socket {}. {:?}",
                client_connection_id, err
            );
        }
    }
}

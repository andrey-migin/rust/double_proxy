pub enum TcpClientSocketCommand {
    Connect {
        client_connection_id: u16,
        host_port: String,
    },
    Disconnect {
        client_connection_id: u16,
    },
    SendRawPayload {
        client_connection_id: u16,
        raw_data: Vec<u8>,
    },

    ProxySocketIsDisconnected,
}

use crate::{app::client_app_ctx::ClientAppCtx, common::events::Receiver};
use async_std::net::TcpStream;
use futures::{AsyncReadExt, AsyncWriteExt, StreamExt};
use std::sync::Arc;

use super::{
    buffer_reader::BufferReader, connection_ctx::ConnectionContext,
    contracts_serializer::ParseFailResult, events::ProxySocketEvent, proxy_socket_contracts::*,
};

pub async fn start(
    host_port: String,
    app_ctx: Arc<ClientAppCtx>,
    proxy_socket_receiver: Receiver<ProxySocketEvent>,
) {
    let mut connection_id: u16 = 0;
    async_std::task::spawn(proxy_tcp_clients_events_loop(
        proxy_socket_receiver,
        app_ctx.clone(),
    ));

    loop {
        println!("Connecting to proxy socket: {}", host_port);
        let connect_result = TcpStream::connect(host_port.as_str()).await;

        match connect_result {
            Ok(tcp_stream) => {
                println!("Connected to proxy socket: {}", host_port);

                connection_id += 1;

                let tcp_stream = Arc::new(tcp_stream);

                let connection_context = Arc::new(ConnectionContext::new(connection_id));

                app_ctx.proxy_server_is_connected(connection_id, tcp_stream.clone());

                async_std::task::spawn(ping_loop(tcp_stream.clone(), connection_context.clone()));

                let result = proxy_client_sockets_read_loop(
                    tcp_stream.clone(),
                    app_ctx.clone(),
                    connection_context,
                )
                .await;

                if let Err(err) = result {
                    println!(
                        "Proxy socket {} is disconnected. Reason: {}",
                        connection_id, err.message
                    )
                }

                let result = tcp_stream.as_ref().shutdown(std::net::Shutdown::Both);

                if let Err(err) = result {
                    println!(
                        "Can not close proxy socket {}. Reason: {:?} ",
                        connection_id, err
                    );
                }

                app_ctx.proxy_server_is_disconnected(connection_id);
            }

            Err(err) => {
                println!(
                    "Can not connect to the Proxy Socket {}. Reason: {:?}",
                    host_port, err
                );

                let duration = std::time::Duration::new(3, 0);
                async_std::task::sleep(duration).await;
            }
        }
    }
}

async fn proxy_tcp_clients_events_loop(
    mut proxy_socket_receiver: Receiver<ProxySocketEvent>,
    app_ctx: Arc<ClientAppCtx>,
) {
    let mut proxy_socket: Option<Arc<TcpStream>> = None;
    let mut current_socket_id: u16 = 0;
    while let Some(msg) = proxy_socket_receiver.next().await {
        match msg {
            ProxySocketEvent::ProxySocketConnected {
                proxy_socket_id,
                tcp_stream,
            } => {
                proxy_socket = Some(tcp_stream);
                current_socket_id = proxy_socket_id;
            }
            ProxySocketEvent::ProxySocketDisconnected { proxy_socket_id } => {
                if current_socket_id == proxy_socket_id {
                    proxy_socket = None;
                }
            }

            ProxySocketEvent::RawPayload {
                client_connection_id,
                payload,
            } => {
                if let Some(tcp_stream) = &proxy_socket {
                    let result = tcp_stream.as_ref().write_all(&payload).await;

                    if result.is_err() {
                        println!("Could send message through write to the proxy socket {} for public socket {}", current_socket_id, client_connection_id);
                    }
                } else {
                    app_ctx.disconnect_client_connection(client_connection_id);
                    println!("Attempt to send to the public socket {} is failed. There is not proxy socket connection established", client_connection_id);
                }
            }
        }
    }
}

async fn proxy_client_sockets_read_loop(
    tcp_stream: Arc<TcpStream>,
    app_ctx: Arc<ClientAppCtx>,
    connection_ctx: Arc<ConnectionContext>,
) -> Result<(), ProxySocketError> {
    let mut buffer_reader = BufferReader::new(crate::common::consts::BUFFER_SIZE * 2);

    let mut tcp_stream: &TcpStream = &*tcp_stream.as_ref();

    loop {
        let buffer_to_read = buffer_reader.borrow_mut();

        let read_size = tcp_stream.read(buffer_to_read).await?;

        if read_size == 0 {
            connection_ctx.disconnect().await;
            return Err(ProxySocketError::from_string(
                "Proxy socket is disconnected. Read size is 0",
            ));
        }

        buffer_reader.commit_written_size(read_size);
        connection_ctx.update_last_read_time().await;
        let mut borrowed_buffer = buffer_reader.get_buffer_to_use();

        while let Some(buffer) = borrowed_buffer {
            let parse_result = ProxySocketContract::deserialize(buffer);

            match parse_result {
                Ok(packet) => {
                    /*
                                       if buffer[0] != SOCKET_PING {
                                           println!(
                                               "Proxy: Server->Client, payload {}. Len: {}",
                                               get_packet_name(buffer[0]),
                                               packet.read_size
                                           );
                                       }
                    */
                    buffer_reader.commit_read_size(packet.read_size);

                    match packet.contract {
                        ProxySocketContract::Payload {
                            client_connection_id,
                            payload,
                        } => {
                            app_ctx
                                .as_ref()
                                .send_payload_to_client_connection(client_connection_id, payload);
                        }
                        ProxySocketContract::Disconnect {
                            client_connection_id,
                        } => {
                            app_ctx
                                .as_ref()
                                .disconnect_client_connection(client_connection_id);
                        }
                        ProxySocketContract::Connect {
                            client_connection_id,
                            host_port,
                        } => {
                            app_ctx
                                .as_ref()
                                .start_new_client_connection(client_connection_id, host_port);
                        }
                        ProxySocketContract::Ping => {}
                    }
                }
                Err(err) => match err {
                    ParseFailResult::Fail => {
                        return Err(ProxySocketError::from_string(
                            "Socket is disconnected because wrong data in the socket",
                        ));
                    }
                    ParseFailResult::NotEnoughDataToReadWrite => {
                        break;
                    }
                },
            }

            borrowed_buffer = buffer_reader.get_buffer_to_use();
        }
    }
}

async fn ping_loop(tcp_stream: Arc<TcpStream>, connection_ctx: Arc<ConnectionContext>) {
    let duration = std::time::Duration::new(3, 0);

    while connection_ctx.is_connected().await {
        async_std::task::sleep(duration).await;

        let last_read_seconds = connection_ctx.get_last_read_seconds().await;

        if last_read_seconds > 9 {
            connection_ctx.disconnect().await;
            break;
        }

        if last_read_seconds > 3 {
            let tcp_contract =
                crate::proxy_socket::proxy_socket_contracts::ProxySocketContract::Ping;

            let payload = tcp_contract.serialize();

            let result = tcp_stream.as_ref().write_all(&payload).await;

            if let Err(err) = result {
                println!(
                    "Can not send ping package to the proxy socket: {}. Reason {:?}. Disconnecting socket",
                    connection_ctx.id,
                    err
                );
                connection_ctx.disconnect().await;
                break;
            }
        }
    }
}

struct ProxySocketError {
    message: String,
}

impl ProxySocketError {
    fn from_string(str: &str) -> Self {
        Self {
            message: str.to_string(),
        }
    }
}

impl From<std::io::Error> for ProxySocketError {
    fn from(err: std::io::Error) -> Self {
        return Self {
            message: err.to_string(),
        };
    }
}
